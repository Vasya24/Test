window.onload = () => {
    class Slider {
        constructor() {
            this.main = document.querySelector(`.main`);
            this.slidesY = this.main.getElementsByClassName(`slide`);
            this.slideCount = 0;
            this.ice = this.main.querySelectorAll(`.iceberg`);
            this.range = this.main.querySelector(`#range`);
            this.calcPages();
            this.nextSlide();
            this.changeXSlides();
            this.timer();
            this.showLeftMenu();
        }

        calcPages() {
            this.pages = this.main.querySelector('.pages');
            for (let i = 0; i < this.slidesY.length; i++) {
                const li = document.createElement(`li`);
                li.classList.add('not-current');
                this.pages.append(li);
            }
            this.li = this.pages.getElementsByTagName(`li`);
            this.li[0].classList.add('current-page');
        }

        nextSlide() {
            const currentPage = n => {
                for (let i = 0; i < this.li.length; i++) {
                    this.li[i].classList.remove('current-page');
                }
                this.li[n].classList.add('current-page');
            }


            const slide = () => {
                window.scrollBy({
                    top: this.slidesY[this.slideCount]
                        .getBoundingClientRect().y,
                    behavior: `smooth`
                })
                currentPage(this.slideCount);
            }

            const iceUp = () => {
                this.ice.forEach(elem => {
                    if (this.slideCount < 1) {
                        elem.classList.remove(`ice-up`)
                        document.querySelector('.one').classList.remove('hide-down');
                    } else {
                        elem.classList.add(`ice-up`);
                        document.querySelector('.one').classList.add('hide-down');
                    }
                })
            }

            const swapSlide = dir => {
                if (dir > 0) {
                    this.slideCount += this.slideCount >= this.slidesY.length - 1 ?
                        0 : 1;
                    slide();
                } else {
                    this.slideCount -= this.slideCount <= 0 ? 0 : 1;
                    slide();
                }
                iceUp();
            }

            window.onwheel = e => swapSlide(e.deltaY)

            let event = null;
            let direction = null;
            window.ontouchstart = e => {
                event = e;
                window.ontouchmove = e => {
                    direction = e.touches[0].pageY - event.touches[0].pageY;
                }
            }

            window.ontouchend = e => {
                swapSlide(-direction)
                event = null;
            }
        }

        changeXSlides() {
            this.range.value = this.range.max;
            const fullRange = this.main.querySelector(`.full-range`);

            const getFull = () => {
                fullRange.style = `
            width: ${(+this.range.value / this.range.max) * 100}%;
            `;
            };
            getFull();

            const xSlidesBox = this.main.querySelector(`.hor-slides`);
            let counter = 3;
            this.range.oninput = () => {
                getFull();
                counter = Math.round(((+this.range.value) / this.range.max) * 2);
                xSlidesBox.style = `
            transform: translateX(-${(100 / 3) * counter}%);
            `;
            }
        }

        timer() {
            const timer = this.main.querySelector(`.timer`);
            const minSpan = timer.querySelector(`.min`);
            const secSpan = timer.querySelector(`.sec`);
            let min = 9;
            let sec = 59;

            const setZero = n => `0${n}`;

            const startTimer = () => {
                sec--;
                if (sec < 0) {
                    sec = 59;
                    min--;
                } else if (min <= 0 && sec <= 0) clearInterval(timerGo)

                minSpan.textContent = min < 10 ? setZero(min) : min;
                secSpan.textContent = sec < 10 ? setZero(sec) : sec;
            }

            const timerGo = setInterval(startTimer, 1000);
        }

        showLeftMenu() {
            const button = this.main.querySelector(`.button`);
            const ul = this.main.querySelector('.menu');

            let hideMenu;
            const startTimer = () => {
                hideMenu = setTimeout(() => {
                    ul.classList.add('hide-menu');
                }, 5000);
            }

            const dontHideMenu = () => clearTimeout(hideMenu);

            let show = false;
            button.onmousedown = () => {
                show = !show;
                show ? ul.classList.remove('hide-menu') :
                    ul.classList.add('hide-menu');
            };

            button.onmouseup = startTimer;
            ul.onmousemove = dontHideMenu;
            ul.onmouseout = startTimer;
            ul.ontouchmove = dontHideMenu;
            ul.ontouchend = startTimer;
        }
    }

    new Slider();
}